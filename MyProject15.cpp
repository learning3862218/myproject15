﻿#include <iostream>

void PrintOddNumbers(int Limit, bool IsOdd)
{
    for (int i = IsOdd ? 0 : 1; i <= Limit; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main()
{
    const int Lim = 15;
    PrintOddNumbers(Lim, false);
}
